#https://www.citypopulation.de/USA-Maine.html

'''
1. Lat/Long - Nearby cities. -Read Excel
2. Find State of the coordinate. -get_coordinates(state)
3. Find all the cities in that state. -Scrape url to get all the cities(TO DO)
4. Get the co-ordiantes of all the cities. -get_coordinates(cities)
5. Find the distance from target lat/long to all the cities lat/long. -calculate_distance()
6. Save top 10 cities.
'''

import time
import requests
import geopy.distance
from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
import xlrd
from xlutils.copy import copy
import csv


def import_excel(name):
	book = xlrd.open_workbook(name)
	sheet = book.sheet_by_index(0)
	state = str(sheet.row_values(1)[0])
	
	#duplicate excel to edit	
	wb = copy(book)
	wb.save(state+'_.xls')
	##

	place_names=[]
	j=2
	for k in range(1,sheet.nrows):
	    place_names.append(str(sheet.row_values(k)[j-1]))
	
	print("---***",len(place_names))
	return(state,place_names)
	

def get_coordinates(name):
	url = "https://us1.locationiq.com/v1/search.php"
	data = {
	    'key': '20e5ac0d175d8f',
	    'q': name,
	    'format': 'json'
	} #different token 4f7d25d653aa22
	response = requests.get(url, params=data)
	response = json.loads(response.text)
	temp = json.dumps(response,indent=4,sort_keys=True)
	temp=json.loads(temp)
	#print(temp)
	temp = temp[0]['boundingbox']
	x =  (float(temp[0])+float(temp[1]))/2
	y =  (float(temp[2])+float(temp[3]))/2
	time.sleep(0.5)
	return(x,y)


def get_cities(state):
	book = xlrd.open_workbook("cities\\"+state+"-cities.xlsx")
	sheet = book.sheet_by_index(0)
	#state = str(sheet.row_values(2)[2])

	cities=[]
	j=1
	for k in range(1,sheet.nrows):
	    cities.append(str(sheet.row_values(k)[j-1]))
	print("\n")
	print(cities)


def calculate_distance(x1,y1,x2,y2):
	c1=(x1,y1)
	c2=(x2,y2)
	return(geopy.distance.distance(c1,c2).km)


def balance():
	url = "https://us1.locationiq.com/v1/balance.php"
	data = {'key': '20e5ac0d175d8f'}
	response = requests.get(url, params=data)
	return(json.loads(response.text)['balance']['day'])


def driver():
	names_lst=['Alabama',"Alaska","Arizona","Arkansas","California","District of Columbia","Florida","Idaho","Indiana","Iowa","Maryland","Massachusetts","Michigan","Mississippi","Missouri","Montana","Nebraska","Nevada","New Mexico","New York","North Carolina","Oklahoma","Pennsylvania","South Dakota","Tennessee","Texas","Utah","Virginia","Wisconsin","Wyoming"]
	names_lst=["Pennsylvania","South Dakota","Tennessee","Texas","Utah","Virginia","Wisconsin","Wyoming"]
	
	#names_lst=['test']
	for name_ in names_lst:
		state,cities=import_excel("cities\\"+name_+"-cities.xlsx")
		print("----",state,balance())
	
		rb = xlrd.open_workbook(state+"_.xls")
		wb = copy(rb)
		s = wb.get_sheet(0)

		cities_co={}
		for city in cities:
			print(cities.index(city),len(cities))
			try:
				cities_co[city]=get_coordinates(city+","+state)
			except:
				print("---query per min limit reached, paused for 30 sec---",city,balance())
				time.sleep(30)
				cities_co[city]=get_coordinates(city+","+state)

		row_no=1
		for city_parent in cities:
			#print(row_no,len(coordinates))
			top20=[]
			
			##loop lat lang
			top20.append(cities_co[city_parent][0])
			top20.append(cities_co[city_parent][1])			


			print(top20)
			#top10=", ".join(top10)
			col_no=3
			for top_state in top20: #diff state in diff col
				s.write(row_no,col_no,top_state)
				col_no+=1
			row_no+=1
		wb.save(state+'_output.xls')
		print(state+'_output.xls Saved Successfully')
	

driver()
