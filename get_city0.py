#https://www.citypopulation.de/USA-Maine.html

'''
1. Lat/Long - Nearby cities. -Read Excel
2. Find State of the coordinate. -get_coordinates(state)
3. Find all the cities in that state. -Scrape url to get all the cities(TO DO)
4. Get the co-ordiantes of all the cities. -get_coordinates(cities)
5. Find the distance from target lat/long to all the cities lat/long. -calculate_distance()
6. Save top 10 cities.
'''

import time
import requests
import geopy.distance
from bs4 import BeautifulSoup
from urllib.request import urlopen
import json
import xlrd
from xlutils.copy import copy
import csv

def import_excel(name,name_):
	print("Working....on...",name_)
	book = xlrd.open_workbook(name)
	sheet = book.sheet_by_index(0)
	state = name_
	#duplicate excel to edit
	wb = copy(book)
	wb.save(state+'_output.xls')
	##

	coordinates = []
	#j=6
	#for k in range(1,sheet.nrows):
	#    coordinates.append([str(sheet.row_values(k)[j-1]),str(sheet.row_values(k)[j])])

	place_names=[]
	j=1
	for k in range(5,sheet.nrows):
		n_ = str(sheet.row_values(k)[j-1])
		try:
			coordinates.append(get_coordinates(n_+","+state))
			time.sleep(0.5)
		except:
			print("---OOPS-Query/min exceede, wait 30 sec--",n_,balance())
			time.sleep(30)
			coordinates.append(get_coordinates(n_+","+state))
		
		place_names.append(n_)
	
	print("---***",len(place_names))
	return(state,coordinates,place_names)


def get_coordinates(name):
	url = "https://us1.locationiq.com/v1/search.php"
	#4f7d25d653aa22
	data = {
	    'key': '20e5ac0d175d8f',
	    'q': name,
	    'format': 'json'
	}
	response = requests.get(url, params=data)
	response = json.loads(response.text)
	temp = json.dumps(response,indent=4,sort_keys=True)
	temp=json.loads(temp)
	#print(temp)
	temp = temp[0]['boundingbox']
	x =  (float(temp[0])+float(temp[1]))/2
	y =  (float(temp[2])+float(temp[3]))/2
	return([x,y])


def get_cities_temp(state):
	url = "https://www.citypopulation.de/USA-"+state.replace(" ","")+".html"
	page=urlopen(url)
	soup = BeautifulSoup(page,'html.parser')
	cities = soup.findAll('span',{'itemprop':'name'})[4:]
	cities = [lol.text for lol in cities]

	#temp_l=cities.index('East Detroit') #FIXXXXXXX
	
	status = soup.findAll('td',{'class':'rstatus'})[1:]
	status = [lol.text for lol in status]

	#status=status[:temp_l]+['null']+status[temp_l:] #FIXXXXXXX

	print("---",len(cities),len(status))
	
	for i in range(len(cities)):
		print(cities[i],"---",status[i])

	city=[]
	for i in range(0,len(cities)):
		if (status[i]=='City'):
			city.append(cities[i])
	print(len(city))
	return(city)

def get_cities(state):
	book = xlrd.open_workbook("Cities\\"+state+" - Cities.xlsx")
	sheet = book.sheet_by_index(0)
	#state = str(sheet.row_values(2)[2])

	cities=[]
	j=1
	for k in range(5,sheet.nrows):
	    cities.append(str(sheet.row_values(k)[j-1]))
	return(cities)



def calculate_distance(x1,y1,x2,y2):
	c1=(x1,y1)
	c2=(x2,y2)
	return(geopy.distance.distance(c1,c2).km)


def balance():
	url = "https://us1.locationiq.com/v1/balance.php"
	data = {'key': '4f7d25d653aa22'}
	response = requests.get(url, params=data)
	return(json.loads(response.text)['balance']['day'])

def driver():

	names_lst=['Connecticut','Delaware','Illinois','Maine','Maryland','Massachusetts','Michigan','New Hampshire','New Jersey','New York','Ohio','Pennsylvania','Rhode Island','Texas']

	for name_ in names_lst:
		state,coordinates,place_names=import_excel("Cities\\"+name_+" - Cities.xlsx",name_)
		print("----",state,len(coordinates),balance())
		state_co=get_coordinates(state)
		cities=get_cities(state)
		


		rb = xlrd.open_workbook(state+"_output.xls")
		wb = copy(rb)
		s = wb.get_sheet(0)

		cities_co={}
		for city in cities:
			try:
				cities_co[city]=get_coordinates(city+","+state)
				time.sleep(0.5)
			except:
				print("---OOPSI-Query/min exceede, wait 30 sec--",city,balance())
				time.sleep(30)
				cities_co[city]=get_coordinates(city+","+state)

		row_no=5
		last_temp=0
		for cood_parent in coordinates:
			print(row_no,len(coordinates),last_temp)
			top10=[]
			temp={}
			for city in cities_co:
				distance=calculate_distance(float(cood_parent[0]),float(cood_parent[1]),float(cities_co[city][0]),float(cities_co[city][1]))
				temp[distance]=city
				#print(distance)
			flag_count=0
			top10.append(float(cood_parent[0]))
			top10.append(float(cood_parent[1]))
			for cood in sorted(temp):
				if flag_count==20:
					break
				kola_temp=temp[cood]
				if not (kola_temp == place_names[last_temp]): #excludes if place_name = state name
					top10.append(kola_temp)
					flag_count+=1
			last_temp+=1
			print(top10)
			#top10=", ".join(top10)
			col_no=13
			for top_state in top10: #diff state in diff col
				s.write(row_no,col_no,top_state)
				col_no+=1
			row_no+=1
		wb.save(state+'_output.xls')
		print(state+'_output.xls Saved Successfully')


driver()
#get_coordinates("Barrington, Rhode Island")